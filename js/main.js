					angular.module('skylux',
						[
						'ngMaterial'
						,'urls'
						,'first_view'
						,'third_view'
						,'backend'
						,'ngResource'
						,'feed'
						,'calcule'])

					   .config(function($mdThemingProvider) {
					  $mdThemingProvider.theme('default')
					    .primaryPalette('pink')
					    .accentPalette('orange');
					});