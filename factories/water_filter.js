"use strict";
angular.module('feed',[])
.factory('plate',function(myrest){
	var data={
		grab_envilonment:function(){
			return myrest.env().query().$promise
		},
		grab_geo:function(){
			return myrest.geo().query().$promise
		},
		grab_products:function(){

			return myrest.products().query().$promise
		},
		got_mesures:function(){

			return myrest.mesure().query().$promise
		},
		grab_light:function(){
			return myrest.light().query().$promise
		},
		join_filter:function(){
			return myrest.flter().query().$promise
		}

      
	}

	
	data.findNum=function(id){
		var rest=myrest.light().query().$promise;
		var prom;
		return rest.then((data)=>{
			for(var i in data){
				if (data[i].id==id) {
					prom=data[i].real_num;
				}
			}

			return prom;
		})

		 
	};

	data.findname=function(prod){
      var allprod=myrest.light().query().$promise
      var rlnum="1";
      return allprod.then((data)=>{
      	for(var x in data){
      		if (data[x].id==prod) {
               rlnum=data[x].real_num;
      		}
      	}

      	return rlnum;
      })
	}


	data.locfilter=function(accord){
		var thelocs=myrest.geo().query().$promise
		var lx=[];

		return thelocs.then((data)=>{
			for(var j in data[1].infom){

				if (data[1].infom[j].idf==accord) {

					lx.push(data[1].infom[j]);

				}
				
			}

			return lx;
		})

	}

	data.capitalfilt=function(cpt,accd){
		var cpts=myrest.geo().query().$promise
		var val;
		return cpts.then((data)=>{
			for(var k in data[1].infom){

				if (data[1].infom[k].capital==cpt&&data[1].infom[k].idf==accd) {

					val=data[1].infom[k];

				}

			}
			return val
		})
	};


	data.dagmnflt=function(dgmt){
		var mdgtm=myrest.mesure().query().$promise
		var pm2;

		return mdgtm.then((mesure)=>{
			for(var m in mesure){
				if (mesure[m].lichmaat==dgmt) {
					pm2=mesure[m];
				}
			}

			return pm2
		})
	}

	return data;
})