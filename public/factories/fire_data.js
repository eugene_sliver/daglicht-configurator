"use strict";
angular.module('backend',[])
.factory('myrest',function(allurls,$resource){
	var water={
		env:function(){
			return $resource(allurls.evilonment)
		}
		,
		geo:function(){
			return $resource(allurls.locations)
		},
		products:function(){
			return $resource(allurls.products)
		},
		mesure:function(){
			return $resource(allurls.mesures)
		},
		light:function(){
			return $resource(allurls.light)
		},
			flter:function(){
				return $resource(allurls.filters)
			}
	}

	return water;
})