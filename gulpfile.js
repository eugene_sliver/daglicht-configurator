var gulp=require('gulp');
var conn=require('gulp-connect');

gulp.task('html',function(){
	gulp.src('views/*.html')
	.pipe(gulp.dest('public/views'))
	.pipe(conn.reload())
});

gulp.task('index',function(){
	gulp.src('index.html')
	.pipe(gulp.dest('public'))
	.pipe(conn.reload())
});
gulp.task('main',function(){
 gulp.src('js/*.js')
 .pipe(gulp.dest('public/js'))
 .pipe(conn.reload())
});
gulp.task('backend',function(){
	gulp.src('js/backend/*.json')
	.pipe(gulp.dest('public/js/backend'))
	.pipe(conn.reload())
});
gulp.task('service',function(){
	gulp.src('services/*.js')
	.pipe(gulp.dest('public/services'))
	.pipe(conn.reload())
});

gulp.task('resources',function(){
	gulp.src('resources/images/*.png')
   .pipe(gulp.dest('public/resources/images'))
   .pipe(conn.reload())
});

gulp.task('component',function(){
	gulp.src('components/*.js')
	.pipe(gulp.dest('public/components'))
	.pipe(conn.reload())
});

gulp.task('factory',function(){
	gulp.src('factories/*.js')
	.pipe(gulp.dest('public/factories'))
	.pipe(conn.reload())
});

gulp.task('css',function(){
	gulp.src('resources/style/*.css')
	.pipe(gulp.dest('public/resources/style'))
	.pipe(conn.reload())

});

gulp.task('route',function(){
	gulp.src('routes/*.js')
	.pipe(gulp.dest('public/routes'))
	.pipe(conn.reload())
});

gulp.task('watch',function(){

gulp.watch('index.html',['index']);
gulp.watch('views/*.html',['html']);
gulp.watch('components/*.js',['component']);
gulp.watch('factories/*.js',['factory']);
gulp.watch('resources/images/*.png',['resources']);
gulp.watch('resources/style/sidecss.css',['css']);
gulp.watch('js/*.js',['main']);
gulp.watch('services/*.js',['service']);
gulp.watch('js/backend/*.json',['backend'])
});

gulp.task('live',function(){
	conn.server({
		root:'.',
		livereload:true
	});
});

gulp.task('default',['watch','live']);